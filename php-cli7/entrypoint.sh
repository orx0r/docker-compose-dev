#!/bin/bash
set -e

# Add local user
USER_ID=${LOCAL_USER_ID:-9001}
USER_NAME=${LOCAL_USER_NAME:-user}

useradd --shell /bin/bash -u $USER_ID -o -c "" -m $USER_NAME
export HOME=/home/$USER_NAME

chown -R $USER_NAME $COMPOSER_HOME

exec gosu $USER_NAME "$@"
