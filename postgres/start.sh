#!/usr/bin/env bash

. ./vars.sh

set -xe

docker run --name $CONTAINER -v $PG_DATA:/var/lib/postgresql/data -p 5432:5432 -d $IMAGE
