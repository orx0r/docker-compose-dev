#!/usr/bin/env bash

. ./vars.sh

set -xe

if [ ! -d "$PG_DATA" ]; then
    mkdir -p $PG_DATA
fi

docker build \
   --build-arg DUMP=$DUMP \
   -t $IMAGE:latest \
   .

# todo проверка на запущенный контейнер
# docker ps --filter "name=intero-postgres" --format "{{.ID}}: {{.Names}}"
# а дальше грепнуть, например

# запуск postgres-server
#docker run --name $CONTAINER -v $PG_DATA:/var/lib/postgresql/data -p 5432:5432 -d postgres

# загрузка БД
#if [ -f "$DUMP" ]; then
#    # СУБД еще не  успела подняться
#    sleep 5s
#    docker run -it --link $CONTAINER:postgres --rm postgres \
#        sh -c 'gunzip -c '"$DUMP"' | psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'
#fi

#/usr/bin/env bash ./stop.sh
