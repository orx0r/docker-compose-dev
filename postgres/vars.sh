#!/usr/bin/env bash

PG_DATA=~/docker/data/postgres
DUMP=full.sql.gz
IMAGE=interoit/postgres
CONTAINER=postgres-dev

export PG_DATA DUMP IMAGE CONTAINER
