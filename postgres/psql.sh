#!/usr/bin/env bash

. ./vars.sh

set -xe

# можно свободно коннектиться командой psql из системы
# например
# psql -h localhost -p 5432 -U postgres -d postgres
# или используем следующую команду если в системе не установлен postgres, которая из образа postgres законнетится через --link

docker run -it --link $CONTAINER:postgres --rm postgres \
    sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'
