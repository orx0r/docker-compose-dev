# preparing

```bash
cp Makefile.dist Makefile
vim Makefile
```

```bash
docker run -it -v $PWD:/docker-entrypoint-initdb.d/ --network composedev_default --rm postgres bash
gunzip -c /docker-entrypoint-initdb.d/full.sql.gz | psql -U postgres -h postgres postgres
```

# hacking

```bash
make up
```
